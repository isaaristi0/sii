package com.codechallenge.sii;

import com.codechallenge.sii.data.payload.Request.EmployeeDTO;
import com.codechallenge.sii.service.EmployeeService;
import com.codechallenge.sii.service.impl.EmployeeServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {

    EmployeeService e = new EmployeeServiceImpl();
    @Test
    public void testEmployee() throws Exception {
        EmployeeDTO employee = e.getEmployeeById(1);
        assertEquals("Tiger Nixon", employee.getEmployee_name());
    }

    @Test
    public void testEmployees() throws Exception {
        List<EmployeeDTO> employee = e.getEmployeesList();
        assertEquals("Tiger Nixon", employee.get(0).getEmployee_name());
    }

}
