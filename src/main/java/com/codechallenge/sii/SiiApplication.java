package com.codechallenge.sii;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class SiiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiiApplication.class, args);
	}

}
