package com.codechallenge.sii.web;

import com.codechallenge.sii.data.payload.Request.EmployeeDTO;
import com.codechallenge.sii.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/employeeList")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<EmployeeDTO> getEmployeesList() throws Exception {
        List<EmployeeDTO> list = employeeService.getEmployeesList();
        return list;
    }

    @GetMapping("/{id}")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<EmployeeDTO> getEmployee(@PathVariable(value="id") Integer id) throws Exception {
        EmployeeDTO employee = employeeService.getEmployeeById(id);
        List<EmployeeDTO> list = new ArrayList<>();
        list.add(employee);
        return list;
    }
}
