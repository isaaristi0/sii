package com.codechallenge.sii.service;

import com.codechallenge.sii.data.payload.Request.EmployeeDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface EmployeeService {
    List<EmployeeDTO> getEmployeesList () throws Exception;
    EmployeeDTO getEmployeeById (Integer id) throws Exception;
}
