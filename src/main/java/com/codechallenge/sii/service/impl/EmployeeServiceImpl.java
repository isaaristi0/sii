package com.codechallenge.sii.service.impl;

import com.codechallenge.sii.data.payload.Request.EmployeeDTO;
import com.codechallenge.sii.data.payload.Response.EmployeeResponse;
import com.codechallenge.sii.data.payload.Response.EmployeesResponse;
import com.codechallenge.sii.service.EmployeeService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Override
    public List<EmployeeDTO> getEmployeesList() throws Exception {
        String uri = "https://dummy.restapiexample.com/api/v1/employees";
        RestTemplate restTemplate = new RestTemplate();

        EmployeesResponse employees = restTemplate.getForObject(uri, EmployeesResponse.class);
        employees.getData().stream().forEach(employeeDTO -> employeeDTO.setEmployee_anual_salary(employeeDTO.getEmployee_salary()*12));

        return employees.getData();
    }

    @Override
    public EmployeeDTO getEmployeeById(Integer id) throws Exception {
        String uri = "https://dummy.restapiexample.com/api/v1/employee/"+id;
        RestTemplate restTemplate = new RestTemplate();

        EmployeeResponse employee = restTemplate.getForObject(uri, EmployeeResponse.class);
        employee.getData().setEmployee_anual_salary(employee.getData().getEmployee_salary()*12);

        return employee.getData();
    }
}
