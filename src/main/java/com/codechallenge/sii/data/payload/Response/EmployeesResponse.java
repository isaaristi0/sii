package com.codechallenge.sii.data.payload.Response;

import com.codechallenge.sii.data.payload.Request.EmployeeDTO;
import lombok.Data;

import java.util.List;

@Data
public class EmployeesResponse {
    private String status;
    private List<EmployeeDTO> data;
    private String message;
}
