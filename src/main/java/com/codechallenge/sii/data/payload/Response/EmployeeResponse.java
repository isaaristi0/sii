package com.codechallenge.sii.data.payload.Response;

import com.codechallenge.sii.data.payload.Request.EmployeeDTO;
import lombok.Data;

@Data
public class EmployeeResponse {
    private String status;
    private EmployeeDTO data;
    private String message;
}
