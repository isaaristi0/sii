package com.codechallenge.sii.data.payload.Request;

import lombok.Data;

@Data
public class EmployeeDTO {
    private Integer id;
    private String employee_name;
    private Double employee_salary;
    private Integer employee_age;
    private String profile_image;
    private Double employee_anual_salary;
}
